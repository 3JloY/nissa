# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process :resize_to_fit => [140, 140]
  end

  version :thumb_300 do
    process :resize_to_fit => [300, 300]
  end

  version :thumb_805 do
    process :resize_to_fill => [805, 384]
  end

  version :thumb_470 do
    process :resize_to_fill => [470, 265]
  end

end
