ActiveAdmin.register Partner do

  permit_params :title, :image, :link, :info
  
  index do
    selectable_column
    column :title
    column :link
    column :image do |partner|
      img src: partner.image_url(:thumb)
    end
    column :info do |partner|
      raw partner.info
    end
    default_actions  
  end

  form do |f|
    f.inputs "Project Details" do
      f.input :title
      f.input :link
      f.input :image, as: :file
      f.input :info, as: :ckeditor
    end
    f.actions
  end

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
