ActiveAdmin.register Activity do

  menu :parent => "Activities"
  permit_params :title, :image, :info, :activity_category_id

  index do
    selectable_column
    column :title
    column :activity_category
    column :image do |activity|
      img src: activity.image_url(:thumb)
    end
    column :info do |activity|
      raw activity.info
    end
    default_actions  
  end

  form do |f|
    f.inputs "Project Details" do
      f.input :title
      # f.input :activity_category_id, as: :select, collection: options_from_collection_for_select(ActivityCategory.all.map {|ac| ac.id ac,title } ), :prompt => "Выберете категорию"
      f.input :activity_category, as: :select, collection: options_from_collection_for_select(ActivityCategory.all, 'id', 'title', activity.activity_category_id), :prompt => "Выберете категорию"
      f.input :image, as: :file
      f.input :info, as: :ckeditor
    end
    f.actions
  end
  
end
