ActiveAdmin.register New do

  permit_params :title, :intro, :full, :image

  index do
    selectable_column
    column :title
    column :intro
    column :full do |new_row|
      raw new_row.full
    end
    column :image do |new_row|
      img src: new_row.image_url(:thumb)
    end
    default_actions  
  end

  form do |f|
    f.inputs "New details" do
      f.input :title
      f.input :intro
      f.input :full, as: :ckeditor
      f.input :image, as: :file
    end
    f.actions
  end
  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
