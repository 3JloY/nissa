ActiveAdmin.register Slide, as: "Slider Slide" do

  permit_params :image
  config.filters = false

  index do
    selectable_column
    column :image do |slide|
      img src: slide.image_url(:thumb)
    end
    default_actions  
  end
   
end
