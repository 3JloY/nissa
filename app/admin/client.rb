ActiveAdmin.register Client do
  menu :parent => "Clients"
  permit_params :title, :client_category_id
  

  form do |f|
    f.inputs "Project Details" do
      f.input :title
      f.input :client_category, as: :select, collection: options_from_collection_for_select(ClientCategory.all, 'id', 'title', client.client_category_id), :prompt => "Выберете категорию"
    end
    f.actions
  end
end
