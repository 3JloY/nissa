ActiveAdmin.register Project do

  permit_params :title, :c_year, :c_month, :image

  months = {"Январь" => 1, "Февраль" => 2, "Март" => 3, "Апрель" => 4, "Май" => 5, "Июнь" => 6, "Июль" => 7, "Август" => 8, "Сентябрь" => 9, "Октябрь" => 10, "Ноябрь" => 11, "Декабрь" => 12 }

  index do
    selectable_column
    column :title do |project|
      raw project.title
    end
    column :c_year
    column :c_month
    column :image do |project|
      img src: project.image_url(:thumb)
    end
    default_actions  
  end

  form do |f|
    f.inputs "Project Details" do
      f.input :title, as: :ckeditor
      f.input :image, as: :file
      f.input :c_year, as: :select, collection: options_for_select((2005..2030).to_a, project.c_year ), :prompt => "Выберете год"
      f.input :c_month, as: :select, collection: options_for_select(months, project.c_month ), :prompt => "Выберете месяц"
    end
    f.actions
  end
  
end
