// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery.bxslider.min.js
//= require turbolinks
//= require fancybox
//= require_tree ./frontside
//= require_self

var slider;

var ready = function() {

  $("a.fancybox").fancybox();

  // карта яндекс
  if ($("#map").length) {
    ymaps.ready(init);
  };

  // партнеры
  $('.clients').on('click', function(e) {
    e.preventDefault();
    drop_down($(this));
  });

  // проекты
  $('.years').find('a').on('click', function(e){
    e.preventDefault();
    year_change($(this));
  });

  $('.projects').on('click', function(e) {
    e.preventDefault();
    drop_down($(this));
  });
  // end проекты

  // slider init
  $('.slides').bxSlider({
    slideWidth: 805,
    auto: true
  });
  // end slider init

  // security slider init
  slider = $('.slider').bxSlider({
    pager: false,
    onSlideAfter: function($slideElement, oldIndex, newIndex) {
      changeActivity(newIndex);
    }
  });

  $('.activity-link').on('click', function(e){
    e.preventDefault();
    $links = $(".activity-link");
    link_index = $links.index($(this));
    
    changeActivity(link_index);

  });
  // end security  slider init
}

$(document).ready(ready);
// $(document).on('page:load', ready);

function changeActivity(index) {
  $links = $(".activity-link");
  $images = $(".activity").find('img');
  
  $links.removeClass('active');
  $images.removeClass('active');

  $links.eq(index).addClass('active');
  $images.eq(index).addClass('active');
  
  slider.goToSlide(index);
}