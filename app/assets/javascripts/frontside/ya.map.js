var myMap, myPlacemark, MyBalloonContentLayoutClass;


function init(){
  
  MyBalloonContentLayoutClass = ymaps.templateLayoutFactory.createClass(
    '<p>г.Петропавловск-Камчатский, ул.Транспортный тупик д.1.</p>'
  );

  myMap = new ymaps.Map("map", {
    center: [53.046055,158.647518],
    zoom: 16
  });

  myPlacemark = new ymaps.Placemark([53.046055,158.647518], 
    { content: 'г.Петропавловск-Камчатский, ул.Транспортный тупик д.1.', balloonContent: 'г.Петропавловск-Камчатский, ул.Транспортный тупик д.1.' },
    { balloonContentLayout: MyBalloonContentLayoutClass } 
  );



  myMap.geoObjects.add(myPlacemark);
}