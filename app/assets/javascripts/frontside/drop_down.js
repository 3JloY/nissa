function drop_down(object) {
  if (object.hasClass('opened')) {
    object.removeClass('opened');
    object.find('li').hide();
  } else {
    object.addClass('opened');
    object.find('li').show();
  };
}


function year_change($object) {
  $years_container = $object.closest('.years');
  $years_container.find(".active").removeClass('active');

  $object.addClass("active");
  current_year = $object.data('year');

  $all_projects = $('.all-projects');
  $all_projects.find('.active').removeClass('active').addClass('hidden');
  $all_projects.find('#'+current_year).removeClass('hidden').addClass('active');
}