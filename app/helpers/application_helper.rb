module ApplicationHelper
  def active(path)
    path = path.split('/')[1] 
    request_path = request.path.split('/')[1] 
    if request_path
      return 'active' unless request_path["#{path}"].blank?
    end
  end
end
