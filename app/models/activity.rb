class Activity < ActiveRecord::Base
  belongs_to :activity_category

  mount_uploader :image, ImageUploader
end
