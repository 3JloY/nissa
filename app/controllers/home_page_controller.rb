class HomePageController < ApplicationController
  def index
    @activity_categories = ActivityCategory.all
    @news = New.paginate(:page => params[:page], :per_page => 4)
  end

  def show
    
  end

end
