class ActivityCategoriesController < ApplicationController
  def show
    @activity_categories = ActivityCategory.find params[:id]
  end
end