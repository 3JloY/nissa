class ProjectsController < ApplicationController
  def index
    
    projects = Project.all

    projects_by_year = projects.group_by { |p| p.c_year }
    
    projects_by_year = projects_by_year.sort_by {|_key, value| -_key}

    @projects_by_year_by_month = Hash.new

    projects_by_year.each do |(key, projects)|
      @projects_by_year_by_month[key] = projects.group_by { |p| p.c_month }
    end

    @months = { 1 => "Январь",  2 => "Февраль",  3 => "Март",  4 => "Апрель",  5 => "Май",  6 => "Июнь",  7 => "Июль",  8 => "Август",  9 => "Сентябрь",  10 => "Октябрь",  11 => "Ноябрь",  12 => "Декабрь" }
  end
end