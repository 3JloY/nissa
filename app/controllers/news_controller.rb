class NewsController < ApplicationController

  def index
    @news = New.paginate(:page => params[:page], :per_page => 12)
  end

  def show
    @single_news = New.find(params[:id])
    @news = New.paginate(:page => params[:page], :per_page => 4)
  end

end