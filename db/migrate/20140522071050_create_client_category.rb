class CreateClientCategory < ActiveRecord::Migration
  def change
    create_table :client_categories do |t|
      t.string :title
      t.timestamps
    end
  end
end
