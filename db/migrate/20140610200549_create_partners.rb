class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :title
      t.string :image
      t.string :link
      t.text :info

      t.timestamps
    end
  end
end
