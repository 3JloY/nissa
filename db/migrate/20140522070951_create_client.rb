class CreateClient < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :title
      t.references :client_category, index: true
      t.timestamps
    end
  end
end
