class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.string :intro
      t.text :full
      t.string :image

      t.timestamps
    end
  end
end
