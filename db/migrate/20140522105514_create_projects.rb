class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.text :title
      t.integer :c_year
      t.integer :c_month

      t.timestamps

    end

    add_index :projects, :c_year
    add_index :projects, :c_month
  end
end
