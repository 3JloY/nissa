class AddSuperadminToUser < ActiveRecord::Migration
  
  def migrate(direction)
    super
    if direction == :up
      u = User.first
      u.superadmin = true
      u.save!
    end
  end

  def change
    add_column :users, :superadmin, :boolean, null: false, default: false
  end

end
