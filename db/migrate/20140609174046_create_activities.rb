class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :title
      t.string :image
      t.text :info

      t.timestamps
    end
  end
end
